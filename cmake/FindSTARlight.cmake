# - Try to find STARlight
# Defines:
#
#  STARLIGHT_FOUND
#  STARLIGHT_HOME (not cached)
#  STARLIGHT_INCLUDE_DIR
#  STARLIGHT_INCLUDE_DIRS (not cached)
#  STARLIGHT_LIBRARY
#  STARLIGHT_LIBRARIES (not cached)
#  STARLIGHT_LIBRARY_DIRS (not cached)

find_path(STARLIGHT_INCLUDE_DIR starlight.h
          HINTS $ENV{STARLIGHT_ROOT_DIR}
          PATH_SUFFIXES include)

if(STARLIGHT_INCLUDE_DIR)
  get_filename_component(STARLIGHT_HOME "${STARLIGHT_INCLUDE_DIR}" DIRECTORY)
  set(STARLIGHT_INCLUDE_DIRS "${STARLIGHT_INCLUDE_DIR}")
  set(STARLIGHT_LIBRARY_DIRS "${STARLIGHT_HOME}/lib")
  set(STARLIGHT_LIBRARIES "${STARLIGHT_LIBRARY_DIRS}/libStarlib.so")
endif()

mark_as_advanced(STARLIGHT_INCLUDE_DIR)
include(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(STARlight DEFAULT_MSG STARLIGHT_INCLUDE_DIRS STARLIGHT_LIBRARIES)
mark_as_advanced(STARLIGHT_FOUND)