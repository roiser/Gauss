!-----------------------------------------------------------------------------
! Package     : Sim/GaussCherenkov
! Responsible	: Sajan Easo
! Purpose     : collection of sensitive detectors, G4 actions,
!               etc for RICH Upgrade
!-----------------------------------------------------------------------------

!2017-01-24 - Sajan Easo 
- For special studies for Future upgrade, the 'Hit time' information is setup to be stored in an ntuple
  in CherenkovG4HistoHitTime.h and CherenkovG4HistoHitTime.cpp.
  It also gets the time and space coordinates of the origin of the charged track into the same ntuple.
  By default all these are switched off.
  In order to activate these from the 'event action', one needs the following lines in the
  options file used for running.  
    GiGa.EventSeq.addTool(CherenkovG4EventAction, name="CkvG4Event") 
    GiGa.EventSeq.CkvG4Event.CkvHistoHitTimeActivate = True
    GiGa.EventSeq.CkvG4Event.CkvHistoHitTimeNtupleFileName=NtupleOutputFileName

!2016-06-15 -Sajan Easo
 - Created a GaussCherenkov Configurable in  python/GaussCherenkov/Configuration.py 
   This has various options in its 'slots'. 

 - This allows the GaussCherenkov to run at 4 different speeeds which can be set from the Gauss Configurable.

   The 'Formula1' option has creation of most of the parameters for RICH specific studies switched off, including
   assigning the location of 'auxiliary classes' like  /Event/MC/Rich/OpticalPhotons at the output. 
   It also has the histogram monitoring switched off. Using this option for standard production may need
   some changes in the RICH reconstrution and overall Gauss Configuration to avoid potential crashes from non-existant classes.
   This option is not used by default for now.

   The 'GTB' option is similar to 'Formula1', but has the monitoring and auxiliary classes switched on. This is the current default option.
   Some of the auxiliary information stored will not be the correct values, but just some defaults. But this is OK for large scale 
   physics simulations where such information is not needed.

   The 'SUV' option is similar to GTB, but has all the auxiliary information created and filled using some step actions. 
   This may be used for RICH specific studies using RICH reconstruction. This is a bit slower than GTB and mostly corresponds to what 
   was activated so far.

   The 'HGV' option has most of the extra processing for RICH studies in simulation activated. There are also options available to 
   activate even more step actions for special simulation studies. 

   The 'clunker' option is available from the Gauss configurable for backward compatibility. This allows the options files used so far, to be activated.
   Hence these  options files are kept in the 'options' area.

   The 'FareFiasco' option allows to switch off the RICH specific processes, including creation of optical photons. This is essentially for the running of
   GAUSS in LHCb upgrade,  without RICH simulation. 
     
  - The Gauss DataType is just 'Upgrade'. So the year of the data for GaussCherenkov configurable set as the year 2022. 
   The aerogel is automatically switched off. The configurable also takes care of the Gauss Configurations 
   where the RICH geometry is not present or the Geant4 is skipped.

  - When the aerogel is switched off in upgrade, in order to avoid some useless print statements there is a file named
   $GAUSSROOT/xml/SimulationRICHAerogelOff.xml which is automatically activated. The older name for this file was SimulationRICHesOff.xml in the same area, 
   which is a confusing name since it does not switch off the RICH ; but this file still kept for backward compatibility.
   

  - The python scripts to activate  'GTB','SUV', 'clunker' and 'FareFiasco' options are expected to be updated in the $APPCONFIGOPTS/Gauss area along 
   with this version. The other options can be activated in a similar way. The 'GTB' is the current default and hence does not require the corresponding
   script in the $APPCONFIGOPTS/Gauss area.

  - At the moment it is does not look to be easy to access the GiGaGeo in this configurable to set Binaryreadout options. But it is possible to access it using the 
    options file. So an options file to set the various options are created in options/GaussCherenkovGiGaGeoOptions/GaussCherenkovBinaryReadout.opts. The default
    setup does not require this options file.


!===================== GaussCherenkov v5r0 2016-06-07 ========================
! 2015-11-17 - Sajan Easo
 - Introduced creation of the hit with new RichSmartID which has now a  flag
   which indicates if the hit is from a largePMT
   Using this RichSmartid needs the latest version of Kernel/LHCbKernel as of
   this date.

! 2015-11-15 - Marco Clemencic
 - Fixed compilation with CLHEP 2.

!===================== GaussCherenkov v4r5 2014-10-08 ========================
! 2014-12-04 - Sajan Easo
 - Fixes to avoid pedantic warnings during compilation.

! 2014-11-16 -Sajan Easo
 -Further modifications to histogram filling for Rich1 occupancy

! 2014-10-15 - Sajan Easo
 - Minor change in the histogram filling for Rich1 occupancy histogram during analysis

! 2014-11-19 - Marco Clemencic
 - Updated CMake configuration

!===================== GaussCherenkov v4r4 2014-10-08 ========================
! 2014-10-07 - Sajan Easo
 - Minor modifications to the Cherenkov analysis in booking and filling
   histograms with new collection set.

! 2014-08-24  - Sajan Easo
 - Added the file GetMCCkvTracksAlg  to avoid problems with collection
   name in that file.
 - Kept some of the factories in Factories.cpp

! 2014-08-24  - Sajan Easo
 - For using the mixed set of PMTS, the sensitive detector and collection name
  configurations are modified. The hits from large PMTS are in a separate
  sensitive detector and are stored in two
  extra collection sets. All this is made transparent to the rest of the
  program.
  Modified several files in the SensDet subdirectory and corresponding .h files.

!===================== GaussCherenkov v4r3 2014-07-28 ========================
! 2014-06-06 - Jibo He
 - GiGaPhysConstructorOpCkv -> RichPmtPhotoElectricEffect -> RichPmtProperties
   . Added options to scale QE for Rich1 and Rich2 separately
 - CherenkovG4HistoFillSet5
   . Added Hists to store true reflection points on Rich2 Mirrors

! 2014-03-20 - Sajan Easo
  -The commit on 2014-01-21 seems to have commented out creation of hits.
   This is now fixed in CkvSensDet.cpp

! 2014-02-13 - Sajan Easo
 - Added options for using more QE tables

!===================== GaussCherenkov v4r2 2014-02-02 ========================
! 2014-01-21 - Paul Szczypka
 - Commented out creation of unused variables.

!2013-12-18 - Sajan Easo
 - Modifications to use mixed set of PMTS (large +standard pixel size) in RICH2
   in   RichPmtProperties,CkvGeometrySetupUtil,CkvSensDet,CherenkovG4CkvRecon,RichG4ReconTransformPmt,
   CherenkovG4HitRecon and CherenkovG4HistoFillSet4
 - Added some prints in RichPmtProperties for reduced set of pmts

!===================== GaussCherenkov v4r1 2013-10-23 ========================
! 2013-10-18 - Jibo He
 - GiGaPhysConstructorOpCkv -> RichPmtPhotoElectricEffect -> RichPmtProperties
  . Added option to "remove" a list of PMTs given in db (set0-2)
 - CherenkovG4HistoDefineSet5.cpp
  . Changed the range of total occupancies histos to 15k to not trunate distributions

!2013-10-12- Sajan Easo
 - Added the modifications to use large PMTs
 - fix for histogram filling  with binary readout.

! 2013-09-23 - Marco Clemencic
 - Added CMake configuration file.

! 2013-08-19 - Jibo He
 - GiGaPhysConstructorOpCkv -> RichPmtPhotoElectricEffect -> RichPmtProperties
  . Added option to scale overall PMT QE

! 2013-08-09 - Sajan Easo
  - Fix couple of compilation warnings in CherenkovG4CkvRecon.cpp which seems to have occured in
    latest update.

! 2013-08-09 - Jibo He
 - GaussCherenkov/CkvG4ReconFlatMirr.h
   src/CherenkovAnalysis/CkvG4ReconFlatMirr.cpp
  . Added functions to calculate distance to flat mirror plane
 - src/CherenkovAnalysis/CherenkovG4CkvRecon
  . Added functions to calculate distance to spherical mirror
 - src/CherenkovAnalysis/CherenkovG4Histo*Set2
  . Added histos of numHits as function of track theta, phi
 - src/CherenkovAnalysis/CherenkovG4Histo*Set4
  . Added histos of emis reso as function of track theta, phi

!===================== GaussCherenkov v4r0 2013-07-24 ========================
! 2013-07-19 - Sajan Easo
 - Adapations for RICH-2019 upgrade studies. Added new files mainly for the
   analysis part.
 - Rearrangeed source code in different subdirectories to avoid compilation
   warnings. Created another source code subdirectory named CkvPhysPhotDet
   as part of re-organization of source code.
 - This is all compatible with Gauss v46r1p1 onwards

!===================== GaussCherenkov v3r1 2013-03-07 ========================
! 2013-02-15 - Patrick Robbe
 - Fix compilation with gcc46

!======================= GaussCherenkov v3r0p1 2013-02-05 ====================
! 2013-01-15 - Patrick Robbe
 - Fix nightly compilation warnings about unused variables

!======================== GaussCherenkov v3r0 2012-11-27 =====================
! 2012-11-09 - Sajan Easo
 - Made some of the modifications made in the code review. Moved some .h files
   to a newly created GaussCherenkov subdirectory and requirements file also
   updated

! 2012-10-18 - Sajan Easo
 - Modified options for scintillation process in RICH2.

! 2012-10-16 - Sajan Easo
 - Added couple of Step Analysis actions for verifying modified RICH optics.
   They are all deactivated by default and are for special studies only.

! 2012-10-03 - Sajan Easo
 - Made adaptations for Geant 4.95.
   These are backward incompatible changes for Geant4 and won't work with
   previous versions of Geant4 and Gauss which uses these earlier versions.

!======================== GaussCherenkov v2r1 2012-07-23 =====================
! 2012-07-23 - Gloria Corti
 - Remove include of files related to ParticlePropertySvc since they are not
   needed
   . src/CherenkovAnalysis/CherenkovG4HistoDefineSet2.cpp
   . src/CherenkovAnalysis/CherenkovG4HistoDefineSet4.cpp
   . src/CherenkovAnalysis/CherenkovG4HistoDefineSet5.cpp
   . src/TorchTestBeamAnalysis/TorchTBG4DefineHistSet6.cpp

! 2012-06-01 - Sajan Easo
 - Added code for Torch Testbeam simulation and  analysis

!======================== GaussCherenkov v2r0 2012-05-14 =====================
! 2012-05-11 - Sajan Easo
 - Adaption for the recent  Gaudi v23. Moved parts of the constructor in
   CkvSensDet to its initialize method.
   Moved also parts of the constructor in CherenkovEventAction to its
   initialize method.  The initialize
   and finalize methods are installed in these two classes as well.

! 2012-03-26 - Sajan Easo
 - Added a some analysis histo modifications.

! 2012-01-27 - Sajan Easo
 - Minor improvements to be to use different PMT photocathode types in RichPmtProperties.cpp and CkvG4GaussPathNames.h

! 2011-11-25 - Sajan Easo
 - Adapting to recent RichDet and SmartID classes.

!======================== GaussCherenkov v1r1p1 2011-10-17 ===================
! 2011-10-17 - Sajan Easo
 - Remove use of Gauss

!========================= GaussCherenkov v1r1 2011-09-20 ====================
! 2011-08-22 - Sajan Easo
 - Comment out the obsolete G4MultipleScattering.hh which caused compilation issues
!========================= GaussCherenkov v1r0 2011-07-24 ====================
! 2011-06-15 -Sajan Easo
 - Made the adaptations needed for the new Radiator number scheme setup in
   GaussRICH

! 2011-05-10 - Sajan Easo
 - Adapted GetMCCkvInfoBase.cpp and .h to use the new base class configuration.

! 2011-05-10 - Sajan Easo
 - Added more histograms for monitoring

! 2011-04-18 - Sajan Easo
 - Added a few histograms on occupancy for monitoring.

! 2011-03-03 - Sajan EASO
 - First version
 - Uses MAPMT as the photodetector
======================== GaussCherenkov v1r0 =================================


!==============================================================================
