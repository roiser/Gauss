################################################################################
# Package: VPMoniSim
################################################################################
gaudi_subdir(VPMoniSim v1r0)

gaudi_depends_on_subdirs(Det/VPDet
                         Event/MCEvent
                         GaudiAlg)

find_package(Boost)
find_package(ROOT)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})

gaudi_add_module(VPMoniSim
                 src/*.cpp
                 LINK_LIBRARIES VPDetLib MCEvent GaudiAlgLib)

