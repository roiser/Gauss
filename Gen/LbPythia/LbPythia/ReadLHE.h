#ifndef LBPYTHIA_READLHE_H
#define LBPYTHIA_READLHE_H 1
// ============================================================================
// Include files
// ============================================================================
// Gaudi
// ============================================================================
// ============================================================================
// Generators
// ============================================================================
#include "Generators/F77Utils.h"
// ============================================================================
// LbPythia
// ============================================================================
#include "LbPythia/Pypars.h"
#include "LbPythia/Pythia.h"
#include "LbPythia/PythiaProduction.h"
// ============================================================================
// Local
// ============================================================================
#include "LbPythia/ReadFile.h"
// ============================================================================
namespace LbPythia
{
  // ==========================================================================
  /** @class ReadLHE RadLHE.h ReadLHE.cpp
   *
   *  Simple production tool which reads parton level
   *  LHE file (Les Houches Events). The LHE standard
   *  in described in hep-ph/0609017 CERN-LCGAPP-2006-03
   *
   *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
   *  @date 2006-10-03
   */
  class ReadLHE: public LbPythia::ReadFile
  {
  public:
    // ========================================================================
    /// tool initialization
    StatusCode initialize () override;
    /// tool finalization
    StatusCode finalize   () override;
    // ========================================================================
    /** standard constructor
     *  @param type tool type (?)
     *  @param name tool name
     *  @param parent tool parent
     */
    ReadLHE
    ( const std::string& type,
      const std::string& name,
      const IInterface* parent ) ;
    // destructor
    virtual ~ReadLHE () ;
    // ========================================================================
  private:
    // ========================================================================
    /// logical file unit  (F77)
    int         m_LUN     ;                         // logical file unit  (F77)
    // ========================================================================
  } ;
  // ==========================================================================
} // end of namespace LbPythia
// ============================================================================
// The END
// ============================================================================
#endif // LBPYTHIA_READLHE_H 1
// ============================================================================
