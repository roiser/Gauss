// ============================================================================
// Include files
// ============================================================================
// Gaudi
// ============================================================================
// ============================================================================
// Local
// ============================================================================
#include "LbPythia/ReadLHE.h"
// ============================================================================
/** @class ReadLHEfileProduction ReadLHEfileProduction.cpp
 *
 *  Simple production tool which reads parton level
 *  LHE file (Les Houches Events). The LHE standard
 *  in described in hep-ph/0609017 CERN-LCGAPP-2006-03
 *
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date 2006-10-03
 */
class ReadLHEfileProduction : public LbPythia::ReadLHE
{
public:
  // =========================================================================
  /** standard constructor
   *  @param type tool type (?)
   *  @param name tool name
   *  @param parent tool parent
   */
  ReadLHEfileProduction
  ( const std::string& type,
    const std::string& name,
    const IInterface* parent )
    : LbPythia::ReadLHE ( type , name , parent )
  {}
  // =========================================================================
} ;
// ============================================================================
// Declaration of the Tool Factory
// ============================================================================
DECLARE_COMPONENT( ReadLHEfileProduction )
// ============================================================================
// The END
// ============================================================================
