// =============================================================================
#ifndef GENERICGENCUTTOOL_H 
#define GENERICGENCUTTOOL_H 1
// =============================================================================
// Include files 
// =============================================================================
// GaudiKernel
// =============================================================================
#include "GaudiKernel/StatEntity.h"
// =============================================================================
// GaudiAlg 
// =============================================================================
#include "GaudiAlg/GaudiHistoTool.h"
// =============================================================================
// AIDA 
// =============================================================================
#include "AIDA/IHistogram2D.h"
// =============================================================================
// Generators 
// =============================================================================
#include "MCInterfaces/IGenCutTool.h"
// =============================================================================
// PartProp
// =============================================================================
#include "Kernel/iNode.h"
// =============================================================================
// LoKi
// =============================================================================
#include "LoKi/GenTypes.h"
// =============================================================================
namespace LoKi 
{
  // ===========================================================================
  /** @class   GenCutTool 
   *  
   *  Simple generic implementation of IGenCutTool interface to check if the 
   *  marked daughter are "good", e.g. in LHCbAcceptance  
   *
   *  @code
   *                                            
   *  from Cofigurable import LoKi__GenCutTool as GenCutTool 
   * 
   *  myalg.addTool ( GenCutTool , 'MyGenCutTool' ) 
   * 
   *  myAlg.MyGenCutTool.Decay = " [B0 => ^pi+ ^pi-]CC "
   *
   *  myAlg.MyGenCutTool.Cuts = { 
   *   '[pi+]cc'  : " in_range( 0.010 , GTHETA , 0.400 ) "
   *  }
   *  
   *  @endcode 
   *
   *  One can specify different crietria for different particles,
   *  ignore certain particles (e.g. neutrinos) etc...
   *
   *  @code
   *                                            
   *  from Cofigurable import LoKi__GenCutTool as GenCutTool 
   * 
   *  myalg.addTool ( GenCutTool , 'MyGenCutTool' ) 
   * 
   *  myAlg.MyGenCutTool.Decay = " [B0 -> ( K*(892)0 => ^K- ^pi+) gamma ]CC "
   *
   *  ## no cuts for gamma
   *  myAlg.MyGenCutTool.Cuts = { 
   *   '[pi+]cc'  : " in_range( 0.010 , GTHETA , 0.400 ) "
   *   '[K+]cc'   : " in_range( 0.010 , GTHETA , 0.300 ) "
   *  }
   *
   *  @endcode 
   *
   *  The tool also produce the generator cut efficiency in form of 2D-histogram,
   *  the exes and binning is specified via the confiuration.
   *  The histogram can be cofigured therough property:
   *  @code
   * 
   *  ## efficiency 2D-histogram: pt (in GeV) versus rapidity:
   *  myAlg.MyGenCutTool.XAxis= ( 0.0 , 10.0 , 20 , 'GPT/GeV') 
   *  myAlg.MyGenCutTool.YAxis= ( 2.0 ,  5.0 ,  6 , 'GY'     ) 
   *  
   *  ## efficiency 2D-histogram: pt ( in GeV) versus pseudo-rapidity:
   *  myAlg.MyGenCutTool.XAxis= ( 0.0 , 10.0 , 20 , 'GPT/GeV' ) 
   *  myAlg.MyGenCutTool.YAxis= ( 2.0 ,  5.0 ,  6 , 'GETA'    ) 
   *  
   *  @endcode 
   *  @see Gaudi::Histo1DDef  
   * 
   *  @see IGenCutTool
   *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
   */
  class GenCutTool : 
    public         GaudiHistoTool , 
    public virtual     IGenCutTool
  {
    // =========================================================================
  public:
    // =========================================================================
    /// Initialization
    StatusCode initialize () override ;
    /// Finalization 
    StatusCode finalize   () override ;
    /** Accept events with 'good' particles 
     *  @see IGenCutTool::applyCut.
     */
    virtual bool applyCut
    ( ParticleVector&              particles       , 
      const HepMC::GenEvent*    /* theEvent     */ , 
      const LHCb::GenCollision* /* theCollision */ ) const override ;
    // =========================================================================
  public:
    // =========================================================================
    /** standard constructor 
     *  @param type the tool type (???)
     *  @param name the actual instance name 
     *  @param parent the tool parent 
     */
    GenCutTool 
    ( const std::string&  type   ,                     // the tool type (???)
      const std::string&  name   ,                     // the tool isntance name 
      const IInterface*   parent ) ;                   // the tool parent 
    /// virtual
    virtual ~GenCutTool ();                // virtual destructor
    // =========================================================================
  protected:
    // =========================================================================
    /// accept the particle 
    bool accept ( const HepMC::GenParticle* particle ) const ;
    // =========================================================================
  protected:
    // =========================================================================
    /// update-handler for the property "Decay" 
    void updateDescriptor ( Property& /* p */ ) ;
    /// update-handler for the properties "Cuts","Preambulo","Factory" 
    void updateCuts       ( Property& /* p */ ) ;
    /// update-handler for the properties "XAxis","YAxis"
    void updateHistos     ( Property& /* p */ ) ;
    // =========================================================================
  private:
    // =========================================================================
    /// decode the decay descriptor 
    StatusCode decodeDescriptor ()  const ;
    /// decode cuts 
    StatusCode decodeCuts       ()  const ;
    /// decode histograms 
    StatusCode decodeHistos     ()  const ;
    // =========================================================================
  private:
    // =========================================================================
    struct TwoCuts
    {
      TwoCuts ( const Decays::iNode&         c1 ,
                const LoKi::GenTypes::GCuts& c2 ) ;
      TwoCuts () ;
      // =======================================================================
      /// the first  cut (particle selector) 
      Decays::Node          first   ; // the first  cut (particle selector) 
      /// the second cut (e.g. acceptance criteria) 
      LoKi::GenTypes::GCut  second  ; // the second cut (e.g. acceptance criteria) 
      /// The counter  
      StatEntity*           counter ; // the counter 
      // =======================================================================
    } ;
    /// vector of cuts 
    typedef std::vector<TwoCuts>  VTwoCuts ; // vector of cuts 
    // =========================================================================
  private:
    // =========================================================================
    /// construct preambulo string 
    std::string preambulo() const ;
    /// calculate the efficiency histo 
    StatusCode getEfficiency() ;
    // =========================================================================
  private:
    // =========================================================================
    /// the decay descriptor 
    std::string                        m_descriptor ;   // the decay descriptor 
    /// the decay finder 
    mutable Decays::IGenDecay::Finder  m_finder     ;   // the decay finder
    /// the LHCb-Acceptance cuts 
    typedef std::map<std::string,std::string>   CMap ;
    CMap                               m_cuts       ; // the 'LHCb-Acceptance cuts
    /// the list of criteria 
    mutable VTwoCuts                   m_criteria   ;   // the list of criteria 
    // =========================================================================    
    /// the preambulo 
    std::vector<std::string>           m_preambulo  ;   // the preambulo 
    /// the factory 
    std::string                        m_factory    ;   // the factory 
    /// fill "efficiency" counters? 
    bool                               m_fill_counters ; // fill counters?
    /// filter?
    bool                               m_filter     ; // filter decays?
    // =========================================================================    
    /// the x-axis for the histogram 
    Gaudi::Histo1DDef              m_xaxis ; // the x-axis for the histogram 
    /// the y-axis for the histogram 
    Gaudi::Histo1DDef              m_yaxis ; // the y-axis for the histogram 
    /// the x-value for histogram 
    mutable LoKi::GenTypes::GFun   m_x     ; // the x-value for histogram 
    /// the y-value for histogram 
    mutable LoKi::GenTypes::GFun   m_y     ; // the y-value for histogram 
    // =========================================================================
    /// the first  histogram ("flow")
    mutable AIDA::IHistogram2D* m_histo1 ;  // the first histogram ("flow")
    /// the second histogram ("accepted")
    mutable AIDA::IHistogram2D* m_histo2 ;  // the second histogram ("accepted")
    /// the third  histogram ("efficiency")
    mutable AIDA::IHistogram2D* m_histo3 ;  // the second histogram ("efficiency")
    // =========================================================================
    mutable bool m_update_decay  ;
    mutable bool m_update_cuts   ;
    mutable bool m_update_histos ;
    // =========================================================================
  } ;
  // ===========================================================================
} //                                                       end of namespace LoKi 
// =============================================================================
//                                                                       The END 
// =============================================================================
#endif // GENERICGENCUTTOOL_H
// =============================================================================
